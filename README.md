## gitee hands on
### Lesson 1 git&gitee tutorial 
#### Register Gitee 
https://gitee.com/ 
#### Install git tool for Windows
https://www.git-scm.com/download/win
#### Git tutorial 


`git clone https://gitee.com/open-robotics/gitee-hands-on.git`

Type user name and password (name: i.e. open-source-robotics with hyphen '-')

```
git status 
git add Foldername/        (git add .) 
git commit -m ‘Update on Nov. 2’  
git push 
```
First time 
```
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```





### Lesson 2  back_history_revert
How to go back to history using git revert, it could be used as managing thesis， or journal writting. 
